﻿using UnityEngine;

namespace MiscUtils
{
    public class FontManager
    {
        private static string _fontName = "Sans";
        private static Font _cache = null;

        public static Font Create(int fontSize = 40)
        {
            if (_cache != null) return _cache;
            return _cache = Font.CreateDynamicFontFromOSFont(_fontName, fontSize);
        }

        public static void SetFontName(string name)
        {
            _fontName = name;
        }

        public static void TearDown()
        {
            _cache = null;
        }
    }
}

