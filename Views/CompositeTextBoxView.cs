﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;


public class CompositeTextBoxView : View
{
    public static CompositeTextBoxView Attach(GameObject parent = null)
    {
        var root = new GameObject("composite text box");
        root.transform.parent = parent.transform;
        root.transform.localPosition = Vector3.zero;
        root.transform.localRotation = Quaternion.identity;
        root.transform.localScale = Vector3.one;

        var view = root.AddComponent<CompositeTextBoxView>();

        return view;
    }

    private Color _defaultColor = Color.white;
    private int _defaultFontSize = 0;
    private List<CompositeTextView> _views = new List<CompositeTextView>();

    public CompositeTextBoxView SetUp(
        int fontSize,
        Color color = default)
    {
        _defaultFontSize = fontSize;
        if (color == default(Color))
        {
            _defaultColor = Color.white;
        }


        return this;
    }

    public void AddText(
        string text,
        System.Action<CompositeTextBoxView> callback,
        List<Operation> operations = null)
    {
        _widthCache = 0;

        var elements = new List<TextElement>();
        var splitedTexts = text.Split("@".ToCharArray());
        
        int index = 0;
        Color currentColor = _defaultColor;
        int currentFontSize = _defaultFontSize;
        float nextMargin = 0;
        foreach (var splitedText in splitedTexts)
        {
            if (splitedText.Length > 0)
            {
                elements.Add(new TextElement
                {
                    text = splitedText,
                    textColor = currentColor,
                    fontSize = currentFontSize,
                    textMargin = nextMargin,
                });
                nextMargin = 0;
            }

            var hasOperation = operations != null && operations.Count > index;
            if (hasOperation)
            {
                var operation = operations[index];

                if (operation is ColorChange)
                {
                    currentColor = operation.color;
                }
                else if (operation is FontSizeChange)
                {
                    currentFontSize = operation.fontSize;
                }
                else if (operation is ResetChange)
                {
                    currentColor = _defaultColor;
                    currentFontSize = _defaultFontSize;
                }
                else if (operation is Margin)
                {
                    nextMargin = operation.margin;
                }
                else if (operation is Image)
                {
                    elements.Add(new TextElement
                    {
                        imagePath = operation.imagePath,
                        imageScale = operation.imageScale,
                        imageOffset = operation.imageOffset + Vector3.right * nextMargin
                    });
                    nextMargin = 0;
                }
            }

            index++;
        }

        var defaultPosition = Vector3.zero;
        foreach(var existedView in _views)
        {
            defaultPosition.y += -1 * existedView.height;
        }

        var view = CompositeTextView
            .Attach(GetRoot())
            .SetUp(_defaultColor, defaultPosition);

        var process = new SerialProcess();

        foreach(var element in elements)
        {
            process.Add(finish => {
                bool isImage = element.imagePath.Length > 0;

                if (isImage)
                {
                    view.AddImage(
                        element.imagePath,
                        scale: element.imageScale,
                        offset: element.imageOffset,
                        callback: compositeTextView =>
                        {
                            finish();
                        });
                }
                else
                {
                    view.AddText(
                        element.text,
                        element.fontSize != default ? element.fontSize : _defaultFontSize,
                        element.textColor != default ? element.textColor : _defaultColor,
                        element.textMargin);
                    finish();
                }
            });
        }

        process.Add(finish =>
        {
            Link(view);
            _views.Add(view);
            callback(this);
        }).Flush();

    }


    public CompositeTextBoxView ShowAsCenter()
    {
        foreach (var view in _views)
        {
            view.ShowAsCenter();
        }
        return this;
    }

    public CompositeTextBoxView ShowAsLeftAlignment()
    {
        foreach (var view in _views)
        {
            view.MoveDefaultPosition(new Vector3(-0.5f * width, 0, 0));
            view.ShowAsLeftAlignment();
        }
        return this;
    }

    public CompositeTextBoxView ShowAsRightAlignment()
    {
        foreach (var view in _views)
        {
            view.MoveDefaultPosition(new Vector3(0.5f * width, 0, 0));
            view.ShowAsRightAlignment();
        }
        return this;
    }

    private float _widthCache = 0;
    public float width
    {
        get
        {
            if (_widthCache > 0) return _widthCache;

            float result = 0;
            foreach (var view in _views)
            {
                if (view.width > result) result = view.width;
            }
            return _widthCache = result;
        }
    }


    public class Operation
    {
        public Color color
        {
            get
            {
                var operation = this as ColorChange;
                return operation != null ? operation.parameter : default;
            }
        }

        public int fontSize
        {
            get
            {
                var operation = this as FontSizeChange;
                return operation != null ? operation.parameter : default;
            }
        }

        public float margin
        {
            get
            {
                var operation = this as Margin;
                return operation != null ? operation.parameter : default;
            }
        }

        public string imagePath
        {
            get
            {
                var operation = this as Image;
                return operation != null ? operation.path : default;
            }
        }

        public float imageScale
        {
            get
            {
                var operation = this as Image;
                return operation != null ? operation.scale : default;
            }
        }

        public Vector3 imageOffset
        {
            get
            {
                var operation = this as Image;
                return operation != null ? operation.offset : default;
            }
        }
    }
    public class ColorChange : Operation
    {
        public Color parameter = default;
    }
    public class FontSizeChange : Operation
    {
        public int parameter = default;
    }
    public class ResetChange : Operation
    {
    }
    public class Margin : Operation
    {
        public float parameter = default;
    }
    public class Image : Operation
    {
        public string path = "";
        public float scale = 1f;
        public Vector3 offset = default;
    }


    private class CompositeTextSet
    {
        public CompositeTextView view;
    }

    private class TextElement
    {
        public string text = "";
        public Color textColor = default;
        public int fontSize = default;
        public float textMargin = 0f;

        public string imagePath = "";
        public float imageScale = 1f;
        public Vector3 imageOffset = default;
    }
}
