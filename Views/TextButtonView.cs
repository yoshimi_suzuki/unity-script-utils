﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;

public class TextButtonView : View
{
    public static TextButtonView Attach(GameObject parent = null)
    {
        var root = new GameObject("image button");
        root.transform.parent = parent.transform;
        root.transform.localPosition = Vector3.zero;
        root.transform.localRotation = Quaternion.identity;
        root.transform.localScale = Vector3.one;

        var rects = new List<RectTransform>();

        rects.Add(root.AddComponent<RectTransform>());

        var frameRoot = new GameObject("frame");
        frameRoot.transform.parent = root.transform;
        frameRoot.transform.localPosition = Vector3.zero;
        frameRoot.transform.localRotation = Quaternion.identity;
        frameRoot.transform.localScale = Vector3.one;
        rects.Add(frameRoot.AddComponent<RectTransform>());
        var image = frameRoot.AddComponent<UnityEngine.UI.Image>();
        image.color = new Color(0.7f, 0.7f, 0.7f, 1);

        var textRoot = new GameObject("text");
        textRoot.transform.parent = root.transform;
        textRoot.transform.localPosition = Vector3.zero;
        textRoot.transform.localRotation = Quaternion.identity;
        textRoot.transform.localScale = Vector3.one;
        rects.Add(textRoot.AddComponent<RectTransform>());
        var text = textRoot.AddComponent<UnityEngine.UI.Text>();
        text.color = new Color(0, 0, 0, 1);

        var controller = root.AddComponent<Controller>();
        var collider = root.AddComponent<BoxCollider2D>();

        var view = root
            .AddComponent<TextButtonView>()
            .SetFields(
            rects,
            collider,
            text,
            controller
            );

        return view;
    }


    private List<RectTransform> _rects = null;
    private BoxCollider2D _collider = null;
    private UnityEngine.UI.Text _text = null;

    public TextButtonView SetFields(
        List<RectTransform> rects,
        BoxCollider2D collider,
        UnityEngine.UI.Text text,
        Controller controler
        )
    {
        _rects = rects;
        _collider = collider;
        _text = text;
        _controller = controler;

        _text.font = MiscUtils.FontManager.Create();
        _text.alignment = TextAnchor.MiddleCenter;

        return this;
    }


    public TextButtonView RegisterCallback(System.Action callback)
    {
        _controller.OnClicked += () =>
        {
            callback();
        };

        return this;
    }
    

    public TextButtonView SetText(
        string title,
        int fontSize = 40)
    {
        _text.text = title;
        _text.fontSize = fontSize;
        return this;
    }

    public TextButtonView SetPosition(
        Position position
    )
    {
        GetRoot().transform.localPosition = position.ToVector3();
        return this;
    }

    public TextButtonView SetSize(
        float w,
        float h
    ) {
        foreach(var rect in _rects)
        {
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        }

        _collider.size = new Vector2(w, h);

        return this;
    }

    public TextButtonView SetBold()
    {
        _text.fontStyle = FontStyle.Bold;
        return this;
    }
}
