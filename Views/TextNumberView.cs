﻿using UnityEngine;
using UnityMVC;

public class TextNumberView : View
{
    [SerializeField]
    private UnityEngine.UI.Text _number;

    public const string PrefabPath = "Prefabs/Common/Numbers/TextNumber";

    public static TextNumberView Attach(GameObject parent = null)
    {
        var view = View.Attach<TextNumberView>(PrefabPath);
        if (parent != null) view.SetParent(parent);
        return view;
    }

    public TextNumberView SetUp(
        int fontSize,
        float width
        )
    {
        _number.rectTransform.sizeDelta = new Vector2(width, fontSize * 1.2f);
        _number.fontSize = fontSize;

        return this;
    }

    public TextNumberView SetColor(Color color)
    {
        _number.color = color;

        return this;
    }

    public void Show(int number)
    {
        _number.text = number.ToString();
    }
}
