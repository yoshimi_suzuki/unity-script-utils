﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;

public class ImageNumberView : View
{
    public static ImageNumberView Attach(GameObject parent = null)
    {
        return ViewUtils.Attach<ImageNumberView>(parent);
    }

    private List<Sprite> _sprites = new List<Sprite>();
    private List<DigitView> _views = new List<DigitView>();
    private int _maxDigit = 6;
    private bool _fillZero = false;
    private float _elementWidthRatio = 1.0f;

    private Align _align = Align.None;

    public enum Align
    {
        None = 0,
        Left,
        Center,
        Right,
    }


    public ImageNumberView SetUp(
        List<Sprite> sprites,
        int maxDigit = 6,
        bool fillZero = false,
        Align align = Align.Center,
        float elementWidthRatio = 1.0f,
        Color color = default
        )
    {
        _sprites = sprites;
        _maxDigit = maxDigit;
        _fillZero = fillZero;
        _elementWidthRatio = elementWidthRatio;

        _align = align;

        foreach (var view in _views) view.Detach();
        _views.Clear();

        for (var i =0; i< maxDigit; i++)
        {
            var view = DigitView.Attach(GetRoot()).SetUp(sprites, color);

            if (fillZero)
            {
                view.Show(0);
            }
            else
            {
                view.Hide();
            }

            _views.Add(view);
        }

        UpdateAlign();

        return this;
    }

    public ImageNumberView Show(int number)
    {
        return Show(number.ToString());
    }

    public ImageNumberView Show(string numbers)
    {
        for(
            var i = 0;
            i < _maxDigit;
            i++)
        {
            if (i < numbers.Length)
            {
                ShowNumber(
                    int.Parse("" + numbers[numbers.Length - 1 - i]),
                    i
                    );
            }
            else
            {
                if (_fillZero)
                {
                    ShowNumber(0,i);
                }
                else
                {
                    HideNumber(i);
                }
            }
        }

        UpdateAlign();

        return this;
    }


    private void ShowNumber(int number, int index)
    {
        _views[index].Show(number);
    }

    private void HideNumber(int index)
    {
        _views[index].Hide();
    }



    private void UpdateAlign()
    {
        float origin = 0;
        if (_align == Align.Left)
        {
            origin = 1 * visibleWidth - elementWidth/2;
        }
        else if (_align == Align.Center)
        {
            origin = 1 * visibleWidth * 0.5f;
        }
        else if (_align == Align.Right)
        {
            origin = elementWidth/2;
        }
        float margin = -1 * elementWidth;
        int index = 0;
        foreach (var view in _views)
        {
            if (view.hidden) continue;
            view.transform.localPosition = new Vector3(origin + margin * index++, 0,0);
        }
    }

    public float visibleWidth
    {
        get
        {
            if (_fillZero) return elementWidth * _maxDigit;

            float result = 0;
            foreach(var view in _views)
            {
                if (view.hidden) continue;
                result += elementWidth;
            }
            return result;
        }
    }

    public float elementWidth
    {
        get
        {
            return _sprites[0].rect.width * _elementWidthRatio;
        }
    }

    private class DigitView : View
    {
        public static DigitView Attach(GameObject parent = null)
        {
            return ViewUtils.Attach<DigitView>(parent);
        }

        private List<GameObject> _nodes = new List<GameObject>();
        private bool _hidden = true;

        public DigitView SetUp(List<Sprite> sprites, Color color = default)
        {
            for(var i =0; i < 10; i++)
            {
                var node = ViewUtils.AddChild(GetRoot(), i.ToString());
                var image = node.AddComponent<UnityEngine.UI.Image>();
                image.sprite = sprites[i];
                node.GetComponent<RectTransform>().sizeDelta = new Vector2(sprites[i].rect.width, sprites[i].rect.height);
                node.SetActive(false);

                if (color != default)
                {
                    image.color = color;
                }
                _nodes.Add(node);
            }

            return this;
        }

        public void Show(int number)
        {
            for (var i = 0; i < 10; i++)
            {
                _nodes[i].SetActive(i == number%10 ? true : false);
            }
            _hidden = false;
        }

        public void Hide()
        {
            for (var i = 0; i < 10; i++)
            {
                _nodes[i].SetActive(false);
            }
            _hidden = true;
        }

        public bool hidden
        {
            get
            {
                return _hidden;
            }
        }
    }
}