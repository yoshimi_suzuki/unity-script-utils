﻿using UnityEngine;
using UnityMVC;

public class DummyEffectView : View
{
    [SerializeField]
    private RectTransform[] _rects = null;

    [SerializeField]
    private UnityEngine.UI.Text _text = null;


    public const string PrefabPath = "Prefabs/Common/Effects/DummyEffect";

    
    private System.Action OnFinished = null;
    private float _elaspedSconds = 0;
    private float _duration = 1f;

    public static DummyEffectView Attach(GameObject parent)
    {
        var view = View.Attach<DummyEffectView>(PrefabPath);
        view.SetParent(parent);
        return view;
    }

    public DummyEffectView SetUp(
        float duration,
        System.Action callback)
    {
        _duration = duration;
        OnFinished = callback;
        return this;
    }

    public DummyEffectView SetText(
        string title,
        int fontSize = 40)
    {
        _text.text = title;
        _text.fontSize = fontSize;
        return this;
    }
    public DummyEffectView SetPosition(
        float x,
        float y
        )
    {
        GetRoot().transform.localPosition = new Vector3(x, y, 0);
        return this;
    }

    public DummyEffectView SetSize(
        float w,
        float h
    ) {
        foreach(var rect in _rects)
        {
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, w);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, h);
        }
        
        return this;
    }

    private void Update()
    {
        if (IsFinished()) return;
        _elaspedSconds += UnityEngine.Time.deltaTime;
        if (IsFinished() && OnFinished != null) {
            OnFinished();
            Detach();
        }
    }

    private bool IsFinished()
    {
        return _elaspedSconds >= _duration;
    }
}
