﻿using UnityEngine;
using UnityMVC;

public class NumberView : View
{
    public static TextNumberView AttachTextNumber(GameObject parent = null)
    {
        return TextNumberView.Attach(parent);
    }
}