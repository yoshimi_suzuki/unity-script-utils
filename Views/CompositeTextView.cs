﻿using UnityEngine;
using UnityMVC;

public class CompositeTextView : View
{
    public static CompositeTextView Attach(GameObject parent = null)
    {
        var root = new GameObject("composite text");
        root.transform.parent = parent.transform;
        root.transform.localPosition = Vector3.zero;
        root.transform.localRotation = Quaternion.identity;
        root.transform.localScale = Vector3.one;

        var view = root.AddComponent<CompositeTextView>();

        return view;
    }

    private Color _defaultColor = Color.white;
    private Vector3 _defaultPosition = Vector3.zero;
    private float _offset = 0;
    private float _height = 0;

    public CompositeTextView SetUp(
        Color color = default,
        Vector3 defaultPosition = default)
    {
        if (color == default(Color))
        {
            _defaultColor = Color.white;
        }

        GetRoot().transform.localPosition = _defaultPosition = defaultPosition;

        return this;
    }

    public CompositeTextView MoveDefaultPosition(Vector3 diff)
    {
        _defaultPosition += diff;
        return this;
    }

    public CompositeTextView AddText(
        string text,
        int fontSize,
        Color fontColor = default,
        float margin = 0
    ) {
        if (fontColor == default(Color))
        {
            fontColor = _defaultColor;
        }

        var root = new GameObject("text:" + text);
        root.transform.parent = GetRoot().transform;
        root.transform.localPosition = new Vector3(0, 0, 0);
        root.transform.localRotation = Quaternion.identity;
        root.transform.localScale = Vector3.one;

        var uiText = root.AddComponent<UnityEngine.UI.Text>();
        uiText.font = MiscUtils.FontManager.Create();
        uiText.fontSize = fontSize;
        uiText.alignment = TextAnchor.MiddleLeft;
        uiText.text = text;
        uiText.color = fontColor;

        root.GetComponent<RectTransform>().sizeDelta = new Vector2(uiText.preferredWidth, fontSize * 2);

        _offset += uiText.preferredWidth / 2 + margin;
        root.transform.localPosition = new Vector3(_offset, 0, 0);
        _offset += uiText.preferredWidth / 2;

        if (_height < uiText.preferredHeight)
            _height = uiText.preferredHeight;

        return this;
    }

    public CompositeTextView AddImage(
        string imagePath,
        string rootDirectory = "Images/",
        float width = 0,
        float height = 0,
        float scale = 1.0f,
        Vector3 offset = default,
        System.Action<CompositeTextView> callback = null)
    {
        bool isRawSize = width == 0 || height == 0;

        var composer = new ImageComposer(imagePath).SetRootDirectory(rootDirectory);
        composer.SetParent(GetRoot().transform);
        if (!isRawSize)
        {
            composer.SetSize(width, height);
        }
        composer.Show(rectTransform =>
        {
            rectTransform.sizeDelta *= scale;
            _offset += rectTransform.sizeDelta.x / 2 + offset.x;
            rectTransform.localPosition = new Vector3(_offset, 0, 0);
            _offset += rectTransform.sizeDelta.x / 2;

            if (_height < rectTransform.sizeDelta.y)
                _height = rectTransform.sizeDelta.y;

            if (callback != null) callback(this);
        });
        
        return this;
    }

    public CompositeTextView ShowAsCenter()
    {
        GetRoot().transform.localPosition = _defaultPosition + new Vector3(-1 * _offset / 2, 0, 0);
        return this;
    }

    public CompositeTextView ShowAsLeftAlignment()
    {
        GetRoot().transform.localPosition = _defaultPosition;
        return this;
    }

    public CompositeTextView ShowAsRightAlignment()
    {
        GetRoot().transform.localPosition = _defaultPosition + new Vector3(-1 * _offset, 0, 0);
        return this;
    }


    public float width
    {
        get
        {
            return _offset;
        }
    }

    public float height
    {
        get
        {
            return _height;
        }
    }
}
