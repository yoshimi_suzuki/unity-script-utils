﻿using UnityEngine;
using UnityMVC;

public class TouchScreenAreaView : View
{
    public static TouchScreenAreaView Attach(GameObject parent = null)
    {
        return ViewUtils
            .Attach<TouchScreenAreaView>(parent != null ? parent : ViewManager.Instance.GetRoot())
            .SetUp();
    }

    public static TouchScreenAreaView Attach(
        System.Action callback,
        GameObject parent = null)
    {
        return ViewUtils
            .Attach<TouchScreenAreaView>(parent != null ? parent : ViewManager.Instance.GetRoot())
            .SetUp()
            .RegistarTapCallback(callback);
    }

    public TouchScreenAreaView SetUp()
    {
        _controller = ViewUtils.AddFrame(
            GetRoot(),
            new Vector2(4000, 4000),
            new Color(0, 0, 0, 0));

        return this;
    }

    public TouchScreenAreaView RegistarTapCallback(System.Action callback)
    {
        _controller.OnTouchFinished += (position, duration, distance) => {
            callback();
        };
        return this;
    }
}