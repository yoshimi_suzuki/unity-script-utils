﻿using System.Collections.Generic;

using UnityMVC;

public class Slider
{
    public delegate void EventHandler();
    public event EventHandler OnNobUpdated;
    public event EventHandler OnValueUpdated;

    private float _nobPosition = 0;
    private float _maxNobPosition = 1;
    private float _minNobPosition = 0;

    private int _currentValue = 0;
    private int _maxValue = 1;
    private int _minValue = 0;

    public Slider SetPositionRange(float min, float max)
    {
        _minNobPosition = min;
        _maxNobPosition = max;
        _nobPosition = min;
        if (OnNobUpdated != null) OnNobUpdated();
        return this;
    }

    public Slider SetValueRange(int min, int max)
    {
        _minValue = min;
        _maxValue = max;
        _currentValue = min;
        return this;
    }

    public void SetPosition(float position)
    {
        _nobPosition = position;

        if (_nobPosition > _maxNobPosition)
            _nobPosition = _maxNobPosition;
        if (_nobPosition < _minNobPosition)
            _nobPosition = _minNobPosition;

        var distance = _maxNobPosition - _minNobPosition;
        var ratio = _nobPosition / distance;
        int nearestValue = _minValue + (int)System.Math.Round((_maxValue - _minValue) * ratio);
        _currentValue = nearestValue;

        if (OnNobUpdated != null) OnNobUpdated();
        if (OnValueUpdated != null) OnValueUpdated();
    }

    public void Move(float velocity)
    {
        _nobPosition += velocity;

        if (_nobPosition > _maxNobPosition)
            _nobPosition = _maxNobPosition;
        if (_nobPosition < _minNobPosition)
            _nobPosition = _minNobPosition;

        var distance = _maxNobPosition - _minNobPosition;
        var ratio = (_nobPosition - _minNobPosition) / distance;
        int nearestValue = _minValue + (int)System.Math.Round((_maxValue - _minValue) * ratio);
        _currentValue = nearestValue;

        if (OnNobUpdated != null) OnNobUpdated();
        if (OnValueUpdated != null) OnValueUpdated();
    }

    public void Aplly()
    {
        var distance = _maxNobPosition - _minNobPosition;
        var ratio = (float)_currentValue / (_maxValue - _minValue);
        _nobPosition = _minNobPosition + distance * ratio;

        if (OnNobUpdated != null) OnNobUpdated();
    }


    public int currentValue
    {
        get { return _currentValue; }
    }

    public float nobPosition
    {
        get { return _nobPosition; }
    }

    public float minPosition
    {
        get { return _minNobPosition; }
    }

    public float maxPosition
    {
        get { return _maxNobPosition; }
    }
}
