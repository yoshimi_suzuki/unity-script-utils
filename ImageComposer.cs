﻿using System.Collections.Generic;

using UnityEngine;

public class ImageComposer
{
    public enum AnchorType
    {
        None = 0,
        LeftTop,
        CenterTop,
        RightTop,
        Left,
        Center,
        Right,
        LeftBottom,
        CenterBottom,
        RightBottom,
        Full
    }

    private static uint SerialNumber = 0;
    private static Dictionary<string, Texture2D> _cache = new Dictionary<string, Texture2D>();

    private string _imagePath = "";
    private string _rootDirectory = "Images/";
    private float _x = 0;
    private float _y = 0;
    private float _width = 0;
    private float _height = 0;
    private AnchorType _type = AnchorType.Center;
    private Transform _parent = null;

    public ImageComposer(string imagePath)
    {
        _imagePath = imagePath;
    }
    
    public ImageComposer(
        string imagePath,
        float x,
        float y,
        float width,
        float height,
        AnchorType type,
        Transform parent,
        System.Action<RectTransform> callback = null
    ) {
        _imagePath = imagePath;
        _x = x;
        _y = y;
        _width = width;
        _height = height;
        _type = type;
        _parent = parent;
        Compose(callback);
    }

    public static void ClearCache()
    {
        _cache.Clear();
    }

    public ImageComposer SetPosition(float x, float y)
    {
        _x = x;
        _y = y;
        return this;
    }

    public ImageComposer SetSize(float width, float height)
    {
        _width = width;
        _height = height;
        return this;
    }

    public ImageComposer SetAnchor(AnchorType type)
    {
        _type = type;
        return this;
    }

    public ImageComposer SetParent(Transform parent)
    {
        _parent = parent;
        return this;
    }

    public ImageComposer SetRootDirectory(string directory)
    {
        _rootDirectory = directory;
        return this;
    }

    public void Show(System.Action<RectTransform> callback = null)
    {
        Compose(callback);
    }

    public void Preload(System.Action callback)
    {
        GetTexture(
            _imagePath,
            texture =>
            {
                callback();
            });
    }

    private void Compose(System.Action<RectTransform> callback) {
        GetTexture(
            _imagePath,
            texture => {
                if (texture == null)
                {
                    var errorChild = new GameObject("error" + ++SerialNumber);
                    errorChild.transform.parent = _parent;
                    var errorRectTransform = errorChild.AddComponent<RectTransform>();
                    errorRectTransform.localPosition = Vector3.zero;
                    errorRectTransform.localScale = Vector3.one;
                    callback(errorRectTransform);
                    return;
                }

                var sprite = Sprite.Create(
                    texture,
                    new Rect(0, 0, texture.width, texture.height),
                    Vector2.one / 2);

                var child = new GameObject("image" + ++SerialNumber);
                child.transform.parent = _parent;
                var rectTransform = child.AddComponent<RectTransform>();
                rectTransform.localPosition = Vector3.zero;
                rectTransform.localScale = Vector3.one;

                float width = _width;
                float height = _height;

                if (_type != AnchorType.Full)
                {
                    width = width <= 0 ? texture.width : width;
                    height = height <= 0 ? texture.height : height;
                }

                rectTransform.sizeDelta = new Vector2(width, height);
                rectTransform.anchoredPosition = new Vector2(_x, _y);

                rectTransform.anchorMin = GetAnchorMin(_type);
                rectTransform.anchorMax = GetAnchorMax(_type);

                var imageComponent = child.AddComponent<UnityEngine.UI.Image>();
                imageComponent.sprite = sprite;

                if (callback != null)
                {
                    callback(rectTransform);
                }
            });
    }

    private void GetTexture(
        string imagePath,
        System.Action<Texture2D> callback)
    {
        if (_cache.ContainsKey(imagePath))
        {
            callback(_cache[imagePath]);
            return;
        }

        UnityMVCFileUtil.FileUtil.Instance.loader.LoadBinary(_rootDirectory + imagePath , bytes => {
            if (bytes.Length == 0)
            {
                UnityEngine.Debug.LogError(imagePath + " : error");
                callback(null);
                return;
            }

            if (_cache.ContainsKey(imagePath))
            {
                callback(_cache[imagePath]);
                return;
            }


            var splitedStrings = imagePath.Split(".".ToCharArray()[0]);
            if (splitedStrings.Length == 0) Debug.LogError(imagePath + " has an invalid extension.");

            var ext = splitedStrings[splitedStrings.Length - 1];

            int width = 0;
            int height = 0;

            if (ext == "png")
            {
                long pos = 0;

                byte[] fileSignature = new byte[8];
                for (int i = 0; i < 8; i++, pos++)
                {
                    fileSignature[i] = bytes[pos];
                }

                byte[] chunkSize = new byte[4];
                byte[] chunkType = new byte[4];
                byte[] imageWidth = new byte[4];
                byte[] imageHeight = new byte[4];

                if (System.BitConverter.IsLittleEndian)
                {
                    for (int i = 4; i > 0; i--, pos++)
                    {
                        chunkSize[i - 1] = bytes[pos];
                    }
                    for (int i = 4; i > 0; i--, pos++)
                    {
                        chunkType[i - 1] = bytes[pos];
                    }
                    for (int i = 4; i > 0; i--, pos++)
                    {
                        imageWidth[i - 1] = bytes[pos];
                    }
                    for (int i = 4; i > 0; i--, pos++)
                    {
                        imageHeight[i - 1] = bytes[pos];
                    }
                }
                else
                {
                    for (int i = 0; i < 4; i++, pos++)
                    {
                        chunkSize[i] = bytes[pos];
                    }
                    for (int i = 0; i < 4; i++, pos++)
                    {
                        chunkType[i] = bytes[pos];
                    }
                    for (int i = 0; i < 4; i++, pos++)
                    {
                        imageWidth[i] = bytes[pos];
                    }
                    for (int i = 0; i < 4; i++, pos++)
                    {
                        imageHeight[i] = bytes[pos];
                    }
                }

                width = System.BitConverter.ToInt32(imageWidth, 0);
                height = System.BitConverter.ToInt32(imageHeight, 0);
            }
            else if (ext == "jpg" || ext == "jpeg")
            {
                for (var i = 0; i < bytes.Length - 8; i++)
                {
                    if (
                    bytes[i] == 0xff
                    && (
                        bytes[i + 1] == 0xc0 ||
                        bytes[i + 1] == 0xc1 ||
                        bytes[i + 1] == 0xc2 ||
                        bytes[i + 1] == 0xc3 ||
                        bytes[i + 1] == 0xc5 ||
                        bytes[i + 1] == 0xc6 ||
                        bytes[i + 1] == 0xc7
                    ))
                    {
                        byte[] segmentSize = new byte[2];
                        byte[] imageHeight = new byte[4];
                        byte[] imageWidth = new byte[4];

                        if (System.BitConverter.IsLittleEndian)
                        {
                            segmentSize[0] = bytes[i + 3];
                            segmentSize[1] = bytes[i + 2];
                            imageHeight[0] = bytes[i + 6];
                            imageHeight[1] = bytes[i + 5];
                            imageWidth[0] = bytes[i + 8];
                            imageWidth[1] = bytes[i + 7];
                        }
                        else
                        {
                            segmentSize[0] = bytes[i + 2];
                            segmentSize[1] = bytes[i + 3];
                            imageHeight[0] = bytes[i + 5];
                            imageHeight[1] = bytes[i + 6];
                            imageWidth[0] = bytes[i + 7];
                            imageWidth[1] = bytes[i + 8];
                        }

                        width = System.BitConverter.ToInt32(imageWidth, 0);
                        height = System.BitConverter.ToInt32(imageHeight, 0);
                    }
                }
            }
            else
            {
                Debug.LogError("extension is " + ext + ", this can use .png .jpg .jpeg only.");
            }

            if (width * height == 0) Debug.LogError(imagePath + "'s picture size is invalid.");

            Texture2D texture = new Texture2D(width, height);
            texture.LoadImage(bytes);
            _cache.Add(imagePath, texture);
            callback(texture);
        });
    }

    private Vector2 GetAnchorMin(AnchorType type)
    {
        if (type == AnchorType.LeftTop)
        {
            return new Vector2(0.0f, 0.0f);
        }
        else if (type == AnchorType.CenterTop)
        {
            return new Vector2(0.5f, 0.0f);
        }
        else if (type == AnchorType.RightTop)
        {
            return new Vector2(1.0f, 0.0f);
        }
        else if (type == AnchorType.Left)
        {
            return new Vector2(0.0f, 0.5f);
        }
        else if (type == AnchorType.Center)
        {
            return new Vector2(0.5f, 0.5f);
        }
        else if (type == AnchorType.Right)
        {
            return new Vector2(1.0f, 0.5f);
        }
        else if (type == AnchorType.LeftBottom)
        {
            return new Vector2(0.0f, 1.0f);
        }
        else if (type == AnchorType.CenterBottom)
        {
            return new Vector2(0.5f, 1.0f);
        }
        else if (type == AnchorType.RightBottom)
        {
            return new Vector2(1.0f, 1.0f);
        }
        else if (type == AnchorType.Full)
        {
            return new Vector2(0.0f, 0.0f);
        }

        return Vector2.zero;
    }

    private Vector2 GetAnchorMax(AnchorType type)
    {
        if (type == AnchorType.LeftTop)
        {
            return new Vector2(0.0f, 0.0f);
        }
        else if (type == AnchorType.CenterTop)
        {
            return new Vector2(0.5f, 0.0f);
        }
        else if (type == AnchorType.RightTop)
        {
            return new Vector2(1.0f, 0.0f);
        }
        else if (type == AnchorType.Left)
        {
            return new Vector2(0.0f, 0.5f);
        }
        else if (type == AnchorType.Center)
        {
            return new Vector2(0.5f, 0.5f);
        }
        else if (type == AnchorType.Right)
        {
            return new Vector2(1.0f, 0.5f);
        }
        else if (type == AnchorType.LeftBottom)
        {
            return new Vector2(0.0f, 1.0f);
        }
        else if (type == AnchorType.CenterBottom)
        {
            return new Vector2(0.5f, 1.0f);
        }
        else if (type == AnchorType.RightBottom)
        {
            return new Vector2(1.0f, 1.0f);
        }
        else if (type == AnchorType.Full)
        {
            return new Vector2(1.0f, 1.0f);
        }

        return Vector2.zero;
    }
}
