﻿using System.Collections.Generic;
using UnityEngine;
using UnityMVC;

public class ScrollTabView : View
{
    public static ScrollTabView Attach(GameObject parent)
    {
        return ViewUtils.Attach<ScrollTabView>(parent);
    }

    private Color _defaultFrameColor = new Color(0.4f, 0.4f, 0.4f, 0.6f);
    private Color _defaultKnobColor = new Color(1, 1, 1, 0.9f);

    private UnityEngine.UI.Image _frameImage = null;
    private UnityEngine.UI.Image _knobImage = null;
    private Transform _frameTransform = null;
    private Transform _knobTransform = null;

    private Scroller _scroller = null;
    private float _height;

    public ScrollTabView SetUp(
        Scroller scroller,
        float width,
        float height,
        Color frameColor = default,
        Color knobColor = default)
    {
        _scroller = scroller;
        _height = height;
        if (frameColor != default)
            _defaultFrameColor = frameColor;
        if (knobColor != default)
            _defaultKnobColor = knobColor;

        _frameImage = ViewUtils.AddLine(
            GetRoot(),
            new Vector3(0, -1 * height / 2, 0),
            new Vector3(0, height / 2, 0),
            width,
            _defaultFrameColor);
        _frameTransform = _frameImage.transform;

        _knobImage = ViewUtils.AddLine(
            GetRoot(),
            new Vector3(0, width / 2, 0),
            new Vector3(0, -width / 2, 0),
            width,
            _defaultKnobColor);
        _knobTransform = _knobImage.transform;


        scroller.OnScrollBegan += () => {
            show = true;
            //_frameImage.color = _defaultFrameColor;
            //_knobImage.color = _defaultKnobColor;
        };

        scroller.OnScrollFinished += () => {
            show = false;
            //_frameImage.color = Color.clear;
            //_knobImage.color = Color.clear;
        };

        _frameImage.color = Color.clear;
        _knobImage.color = Color.clear;

        return this;
    }

    private bool show = false;
    private float colorRatio = 0;
    private void Update()
    {
        if (_scroller == null) return;

        UpdateKnobPosition();

        if (show)
        {
            if (colorRatio < 1)
            {
                colorRatio += 0.05f;
            }
            else
            {
                colorRatio = 1;
            }
        }
        else
        {
            if (colorRatio > 0)
            {
                colorRatio -= 0.05f;
            }
            else
            {
                colorRatio = 0;
            }
        }
        _frameImage.color = _defaultFrameColor * colorRatio;
        _knobImage.color = _defaultKnobColor * colorRatio;
    }


    private void UpdateKnobPosition()
    {
        if (_scroller.offsetMax.y - _scroller.offsetMin.y == 0) return;

        var knobHeightRatio = _height / (_scroller.offsetMax.y - _scroller.offsetMin.y);
        var knobHeight = _height * knobHeightRatio;
        _knobImage.GetComponent<RectTransform>().sizeDelta =
            new Vector2(_knobImage.GetComponent<RectTransform>().sizeDelta.x, _height * knobHeightRatio);

        var knobPositionRatio = (1 - _scroller.verticalCurrentOffsetRatio) - 0.5f;
        var knobPositionY = (_height - knobHeight) * knobPositionRatio - knobHeight / 2;
        _knobTransform.localPosition = new Vector3(
            _knobTransform.localPosition.x,
            knobPositionY,
            _knobTransform.localPosition.z);
    }
}