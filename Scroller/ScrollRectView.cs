﻿using System.Collections.Generic;
using UnityEngine;
using UnityMVC;

public class ScrollRectView : View
{
    protected Scroller _scroller = null;
    public ScrollRectView SetScroller(Scroller scroller)
    {
        _scroller = scroller;

        _controller.OnTouchMoved += (diff, duration) => {
            _scroller.AddVelocity(diff.x, diff.y);
        };
        _controller.OnTouchFinished += (position, duration, distance) => {
            _scroller.FinishScrolling();
        };

        return this;
    }

    public ScrollRectView SetVerticalScrollTab(
        Color frameColor = default,
        Color knobColor = default)
    {
        var frameCollider = _controller.GetComponent<BoxCollider2D>();

        var root = ViewUtils.AddChild(GetRoot(), "scroll tab");

        root.transform.localPosition = new Vector3(
            frameCollider.size.x / 2,
            0,
            1);
        ScrollTabView.Attach(root).SetUp(_scroller, 30, frameCollider.size.y, frameColor, knobColor);

        return this;
    }

    public ScrollRectView AssignController(Controller controller)
    {
        _controller = controller;

        return this;
    }

    private void Start()
    {
        UnityEngine.Debug.Assert(_controller != null, "The controller of ScrollRectView must not be null.");
    }

    private void Update()
    {
        if (_scroller != null) _scroller.Tick();
    }
}