﻿using System.Collections.Generic;

using UnityMVC;

public class Scroller
{


    protected List<Model> _models = new List<Model>();
    public System.Func<Model, bool> IgnoreScroll = null;

    protected Position _currentOffset = Position.Create();
    protected Position _offsetMax = Position.Create();
    protected Position _offsetMin = Position.Create();
    protected Position _velocity = Position.Create();
    protected Position _friction = Position.Create();
    protected Position _maxVelocity = Position.Create(10, 10);

    protected bool _horizontal = true;
    protected bool _vertical = true;

    public Scroller SetFriction(float x, float y)
    {
        _friction = Position.Create(x, y);
        return this;
    }

    public Scroller SetLimit(
        float maxX,
        float maxY,
        float minX,
        float minY)
    {
        _offsetMax = Position.Create(maxX, maxY);
        _offsetMin = Position.Create(minX, minY);
        return this;
    }

    public Scroller SetMaxVelocity(float x, float y)
    {
        _maxVelocity = new Position(
            System.Math.Abs(x),
            System.Math.Abs(y)
        );
        return this;
    }

    public Scroller SetOffset(float x, float y)
    {
        var newOffset = Position.Create(x, y);
        if (newOffset.x > _offsetMax.x) newOffset.x = _offsetMax.x;
        if (newOffset.y > _offsetMax.y) newOffset.y = _offsetMax.y;
        if (newOffset.x < _offsetMin.x) newOffset.x = _offsetMin.x;
        if (newOffset.y < _offsetMin.y) newOffset.y = _offsetMin.y;

        var diff = newOffset - _currentOffset;

        foreach(var model in _models)
        {
            model.position += diff;
        }

        _currentOffset = newOffset;
        return this;
    }

    public Scroller AddModel(Model model)
    {
        if (_models.Contains(model)) return this;
        model.position += _currentOffset;
        _models.Add(model);
        return this;
    }

    public Scroller RemoveModel(Model model)
    {
        if (!_models.Contains(model)) return this;
        _models.Remove(model);
        return this;
    }

    public Scroller RemoveModels(List<Model> models)
    {
        foreach (var model in models) RemoveModel(model);
        return this;
    }

    public Scroller AddVelocity(float x, float y)
    {
        _velocity += new Position(
            _horizontal ? x : 0,
            _vertical ? y : 0);
        return this;
    }


    public event Model.EventHandler OnScrollBegan;
    public event Model.EventHandler OnScrollFinished;
    private bool _scrolling = false;
    public void Tick()
    {
        var oldOffset = _currentOffset;
        _currentOffset += _velocity;
        if (_currentOffset.x > _offsetMax.x) _currentOffset.x = _offsetMax.x;
        if (_currentOffset.y > _offsetMax.y) _currentOffset.y = _offsetMax.y;
        if (_currentOffset.x < _offsetMin.x) _currentOffset.x = _offsetMin.x;
        if (_currentOffset.y < _offsetMin.y) _currentOffset.y = _offsetMin.y;

        var diff = _currentOffset - oldOffset;
        foreach(var model in _models)
        {
            if (IgnoreScroll != null && IgnoreScroll(model)) continue;
            model.position = model.position + diff;
        }

        ApplyFrictionX();
        ApplyFrictionY();
        ApplyMaxVelocity();

        var oldScrolling = _scrolling;
        _scrolling = !_currentOffset.FuzzyEquals(oldOffset, 0.1f);

        if (!oldScrolling
            && _scrolling
            && OnScrollBegan != null) OnScrollBegan();
    }

    public void FinishScrolling()
    {
        if (OnScrollFinished != null) OnScrollFinished();
    }

    public bool overMaxX
    {
        get
        {
            return _currentOffset.x >= _offsetMax.x;
        }
    }
    public bool overMinX
    {
        get
        {
            return _currentOffset.x <= _offsetMin.x;
        }
    }
    public bool overMaxY
    {
        get
        {
            return _currentOffset.y >= _offsetMax.y;
        }
    }
    public bool overMinY
    {
        get
        {
            return _currentOffset.y <= _offsetMin.y;
        }
    }


    public float verticalCurrentOffsetRatio
    {
        get
        {
            var min = _offsetMin.y;
            var max = _offsetMax.y;

            var length = max - min;
            var ratio = 1.0f * (currentOffset.y - min) / length;

            return ratio;
        }
    }

    public Position offsetMin
    {
        get
        {
            return _offsetMin;
        }
    }

    public Position offsetMax
    {
        get
        {
            return _offsetMax;
        }
    }

    private void ApplyFrictionX()
    {
        if (_velocity.x == 0) return;
        var x = System.Math.Abs(_friction.x);
        if (_velocity.x > x)
        {
            _velocity.x -= x;
        }
        else if (_velocity.x < -1 * x)
        {
            _velocity.x += x;
        }
        else
        {
            _velocity.x = 0;
        }
    }

    private void ApplyFrictionY()
    {
        if (_velocity.y == 0) return;
        var y = System.Math.Abs(_friction.y);
        if (_velocity.y > y)
        {
            _velocity.y -= y;
        }
        else if (_velocity.y < -1 * y)
        {
            _velocity.y += y;
        }
        else
        {
            _velocity.y = 0;
        }
    }

    private void ApplyMaxVelocity()
    {
        if (_velocity.x > _maxVelocity.x)
        {
            _velocity.x = _maxVelocity.x;
        }
        else if (_velocity.x < -1 * _maxVelocity.x)
        {
            _velocity.x = -1 * _maxVelocity.x;
        }

        if (_velocity.y > _maxVelocity.y)
        {
            _velocity.y = _maxVelocity.y;
        }
        else if (_velocity.y < -1 * _maxVelocity.y)
        {
            _velocity.y = -1 * _maxVelocity.y;
        }
    }


    public Position currentOffset
    {
        get
        {
            return _currentOffset;
        }
    }
}
