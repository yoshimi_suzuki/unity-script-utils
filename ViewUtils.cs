﻿using UnityEngine;
using UnityMVC;

public class ViewUtils
{
    public static Transform InitializeTransform(Transform transform)
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        return transform;
    }
    public static GameObject InitializeTransform(GameObject gameObject)
    {
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.localRotation = Quaternion.identity;
        gameObject.transform.localScale = Vector3.one;
        return gameObject;
    }
    public static GameObject AddChild(GameObject parent, string name = "-")
    {
        var child = new GameObject(name);
        child.transform.parent = parent.transform;
        InitializeTransform(child);
        return child;
    }
    public static GameObject SetParent(GameObject child, GameObject parent, Vector3 position = default(Vector3))
    {
        child.transform.parent = parent.transform;
        InitializeTransform(child);
        child.transform.localPosition = position;
        return child;
    }
    public static T Attach<T>(GameObject parent) where T : View
    {
        var root = new GameObject(typeof(T).ToString());
        root.transform.parent = parent.transform;
        InitializeTransform(root);
        var view = root.AddComponent<T>();
        return view;
    }

    public static UnityEngine.UI.Text AddText(
        GameObject parent,
        Vector2 size,
        int fontSize,
        Vector3 position = default(Vector3),
        Color textColor = default(Color),
        Color frameColor = default(Color),
        TextAnchor anchor = TextAnchor.MiddleCenter,
        string fontName = "Sans",
        System.Action onClicked = null
    ) {
        if (position == default(Vector3))
        {
            position = Vector3.zero;
        }
        if (textColor == default(Color))
        {
            textColor = new Color(1, 1, 1, 1);
        }
        if (frameColor == default(Color))
        {
            frameColor = new Color(0, 0, 0, 0);
        }

        var root = new GameObject("text root");
        root.transform.parent = parent.transform;
        InitializeTransform(root);
        root.transform.localPosition = position;

        var textRoot = new GameObject("text");
        textRoot.transform.parent = root.transform;
        InitializeTransform(textRoot);
        
        textRoot.AddComponent<RectTransform>().sizeDelta = size;

        if (frameColor.a > 0)
        {
            AddFrame(
                root,
                size,
                frameColor).OnClicked += ()=> {
                    if (onClicked != null) onClicked();
                };
            textRoot.transform.SetAsLastSibling();
        }

        var text = textRoot.AddComponent<UnityEngine.UI.Text>();
        text.font = MiscUtils.FontManager.Create();
        text.fontSize = fontSize;
        text.alignment = anchor;
        text.color = textColor;

        return text;
    }

    public static Controller AddFrame(
        GameObject parent,
        Vector2 size,
        Color color
        )
    {
        var root = new GameObject("frame");
        root.transform.parent = parent.transform;
        InitializeTransform(root);
        root.transform.localPosition = Vector3.forward;

        root.AddComponent<RectTransform>().sizeDelta = size;
        root.AddComponent<UnityEngine.UI.Image>().color = color;
        root.AddComponent<BoxCollider2D>().size = size;

        return root.AddComponent<Controller>();
    }

    public static UnityEngine.UI.Image AddLine(
        GameObject parent,
        Vector3 from,
        Vector3 to,
        float width,
        Color color
        )
    {
        var root = new GameObject("frame");
        root.transform.parent = parent.transform;
        InitializeTransform(root);
        root.transform.localPosition = from;

        var diff = to - from;

        var rectTransform = root.AddComponent<RectTransform>();
        rectTransform.sizeDelta = new Vector2(width, diff.magnitude);
        rectTransform.pivot = new Vector2(0.5f, 0);
        var image = root.AddComponent<UnityEngine.UI.Image>();
        image.color = color;

        var rad = diff.x != 0 ? Mathf.Atan2(diff.y, diff.x) : Mathf.PI / 2;
        var rotation = new Quaternion();
        rotation.eulerAngles = new Vector3(0, 0, rad * 180 / Mathf.PI - 90);
        root.transform.localRotation = rotation;

        return image;
    }
}