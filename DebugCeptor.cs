﻿using System.Collections.Generic;

using UnityEngine;
using UnityMVC;

public class DebugCeptor
{
    private static DebugCeptor _instance = null;
    public static DebugCeptor Instance
    {
        get { return _instance ?? (_instance = new DebugCeptor()); }
    }
    private DebugCeptor() { }

    private bool _active = false;
    private DebugCeptorMeow _meow = null;

    private List<System.Action> _debugAction = new List<System.Action> {
        null,
        null,
        null,
    };

    public void Activate(
        GameObject parent,
        int fontSize = 46,
        float width = 600,
        float height = 600)
    {
        _active = true;
        _meow = DebugCeptorMeow.Attach(parent, fontSize,width,height);
        _meow.transform.localPosition += Vector3.up * -80;
    }

    public void Show(string description)
    {
        _meow?.Show(description);
    }
    public void Show()
    {
        _meow?.Show();
    }

    public void Add(string description)
    {
        _meow?.Add(description);
    }

    public void Clear()
    {
        _meow?.Show("");
    }


    public void Hide()
    {
        _meow?.Hide();
    }

    public bool shown
    {
        get
        {
            return _meow && _meow.shown;
        }
    }



    public void RegisterDebugAction(System.Action action, int index = 0)
    {
        if (_debugAction.Count <= index) return;
        _debugAction[index] = action;
    }

    public void FireDebugAction(int index = 0)
    {
        if (_debugAction.Count <= index
            || _debugAction[index] == null) return;
        _debugAction[index]();
    }



    private string _stopWatchTag = "";
    private float _stopWatchStartTime = -1;
    public void StartStopWatch(string tag)
    {
        _stopWatchTag = tag;
        _stopWatchStartTime = Time.realtimeSinceStartup;
    }
    public void FinishStopWatch()
    {
        if (_stopWatchStartTime < 0) return;
        var time = Time.realtimeSinceStartup - _stopWatchStartTime;
        _meow?.Add(_stopWatchTag + ":" + time);
        _stopWatchTag = "";
        _stopWatchStartTime = -1;
    }



    public bool active
    {
        get
        {
            return _active;
        }
    }



    public class DebugCeptorMeow : View
    {
        public static DebugCeptorMeow Attach(
            GameObject parent,
            int fontSize,
            float width,
            float height)
        {
            return ViewUtils.Attach<DebugCeptorMeow>(parent).SetUp(fontSize,width,height);
        }

        private UnityEngine.UI.Text _text = null;

        public DebugCeptorMeow SetUp(
            int fontSize,
            float width,
            float height)
        {

            _text.transform.parent.gameObject.SetActive(false);

            return this;
        }

        public void Show(string description)
        {
            _text.transform.parent.gameObject.SetActive(true);
            _text.text = description;
        }
        public void Show()
        {
            _text.transform.parent.gameObject.SetActive(true);
        }

        public void Add(string text)
        {
            _text.text += "\n" + text;
        }

        public void Hide()
        {
            _text.transform.parent.gameObject.SetActive(false);
        }

        public bool shown
        {
            get
            {
                return _text.transform.parent.gameObject.activeSelf;
            }
        }
    }
}
